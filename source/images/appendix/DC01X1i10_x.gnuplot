plane = 'DC01X1'
iterations = 10
i = sprintf( '%d', iterations )

set terminal postscript;
set output plane.'i'.i.'_x'.'.ps'

set multiplot;
set size 1,0.5;
set origin 0.0,0.5;
set grid;
set xtics 1;
unset key;
set title plane;
set ylabel 'координата X, см';
set xlabel 'номер итерации';
plot plane.'_x.out' using 1:2 w lp lw 2 lc 3 pt 7 ps 2;
set origin 0.0,0.0;
unset title;
set ylabel 'разность координаты Х с предыдущей итерацией, см';
set xlabel 'номер итерации';
plot plane.'_x.out' using 1:3 w lp lw 2 lc 1 pt 7 ps 2;
unset multiplot;


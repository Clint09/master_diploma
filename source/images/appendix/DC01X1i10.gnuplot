plane = 'DC01X1'
iterations = 10
i = sprintf( '%d', iterations )

set terminal postscript;
set output plane.'i'.i.'.ps'
set title 'GM08U1'

set multiplot;
set size 1,0.5;
set origin 0.0,0.5;
set grid;
set xtics 1;
unset key;
set title plane;
set ylabel 'X coordinate';
set xlabel 'iterations';
plot plane.'_x.out' using 1:2 w lp lw 2 lc 3 pt 7 ps 2;
set origin 0.0,0.0;
unset title;
set ylabel 'X residual wrt previous coordinate';
set xlabel 'iterations';
plot plane.'_x.out' using 1:3 w lp lw 2 lc 1 pt 7 ps 2;
unset multiplot;

set multiplot; 
set size 1,0.5; 
set origin 0.0,0.5; 
set title plane;
set ylabel 'Y coordinate';
set xlabel 'iteration';
plot plane.'_y.out' using 1:2 w lp lw 2 lc 3 pt 7 ps 2; 
set origin 0.0,0.0; 
unset title;
set ylabel 'Y residual wrt previous coordinate';
set xlabel 'iteration';
plot plane.'_y.out' using 1:3 w lp lw 2 lc 1 pt 7 ps 2; 
unset multiplot;


CPP=g++
CFLAGS= -Wall -std=gnu++11             \
		$(shell root-config --cflags)  \
        $(shell clhep-config --include)
INC= -Iinclude -I/home/gecko/software/yaml-cpp/include
LIBS= $(shell root-config --libs)      \
	  $(shell clhep-config --libs)     \
	  -lFoam
MPI = mpic++
LATEX = pdflatex

GEN_DIR = ./epgen
SRC = source
BLD = $(SRC)/build

all: diploma.pdf

diploma.pdf: $(SRC)/*.tex
	if [ ! -d $(BLD) ] ; then mkdir -p $(BLD) ; fi
	$(LATEX) -output-directory=$(BLD) $(SRC)/diploma.tex
	$(LATEX) -output-directory=$(BLD) $(SRC)/diploma.tex

clean:
	rm -rf $(BLD)
	rm -f $(SRC)/*.toc $(SRC)/*.aux $(SRC)/*.log 
	rm -f $(SRC)/*.gz $(SRC)/*.pdf $(SRC)/*.dvi
.PHONY: all clean
